$fn=50;
epsilon = 0.1;
//Define the connector variables
thickness_connector = 6.4; //in z direction
width_connector = 8; //in y direction
diameter_hole = 4.8;
distance_hole = 8;
length_connector = distance_hole+width_connector; //in x direction


//Define the beam variables
distance_connectors=50; //will fix the length of the curved beam
width_beam=1; //in the xy plan
thickness_beam=thickness_connector; //in z direction

//first connector
connector();
//second connector
translate([0,distance_connectors,0])connector();

//connector design
module connector(){
difference() {hull(){
    translate([distance_hole,0,0]) cylinder(h=thickness_connector,d=width_connector,center=true);
    cylinder(h=thickness_connector,d=width_connector,center=true);
};
    cylinder (h = thickness_connector, d=diameter_hole, center = true);
    translate ([distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole, center = true);
}}

//the half-circle Beam
translate([length_connector-width_connector/2-epsilon,distance_connectors/2,0]){
difference(){
difference(){
//cylindre externe
cylinder(h=thickness_beam,d=distance_connectors,center=true);
//cylindre interne
cylinder(h=thickness_beam,d=distance_connectors-width_beam,center=true);
}
//cube servant à couper
translate([-distance_connectors/2,0,0]){
cube([distance_connectors,distance_connectors, thickness_beam],center=true);}}
}
