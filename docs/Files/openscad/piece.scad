//use <prandall_tools.scad>;
//stack(5){
//cube([4,8,4]);  
//cube([4,8,4]);  
//}

//facenumber
 
$fn=25;
difference(){
cube([31,15,9]);
    translate([1.45,1.45])
cube([28,12,8]);
}
 translate([3,3])
for (j = [ 0 :  1]) {
for (i = [ 0 :  3]) {
translate([i*8,j*8,9])
cylinder(h=1,r=2);
}
}
translate([7,7])
for (k = [ 0 :  2]) {
   translate([k*8,0])  
difference(){
cylinder(h=8,r=3);
cylinder(h=8,r=2);
}
}

translate([40,0,0]){

$fn=25;
difference(){
cube([31,15,9]);
    translate([1.45,1.45])
cube([28,12,8]);
}
 translate([3,3])
for (j = [ 0 :  1]) {
for (i = [ 0 :  3]) {
translate([i*8,j*8,9])
cylinder(h=1,r=1.5);
}
}

 translate([7,7])
for (k = [ 0 :  2]) {
   translate([k*8,0])  
difference(){
cylinder(h=8,r=3);
cylinder(h=8,r=2);
}
}
}

