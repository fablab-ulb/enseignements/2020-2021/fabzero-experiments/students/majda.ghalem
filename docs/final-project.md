# Final Project
# Récupération informelle des déchets fablab: Creation de bijoux, accessoires... Recyclage creatif

*Ce travail est très important pour moi, je rends hommage à toutes les personnes qui ont perdu leur emploi, leur entreprise ou leur vie à cause de la pandémie Covid19*.

![](../images/covid19.jpg)

# Comment developper une activité commercial au milieu de la pandémie

## Enoncé de problème

L'envie d'avoir une activité commerciale privée et une source d'argent indépendante m'ont toujours pousser a faire un business. j'ai travaillé dure pour mettre cela en evidence et j'ai acheté ma premiere maison commerciale le 20/01/2020 avec un crédit hypothécaire, j'ai pensé a plusieurs projets pour mon magasin qui se trouve au rez de chaussée, un store dedié aux femmes ètait l'idée gagnante avec des vêtements tendances (chine), des bijoux traditionnelle artisanaux (berber maroc), des produits cosmétiques (atlas Maroc) et les faux cils, extension de cheveux (Thailand).

Une pandémie qui touche le monde entier, le gouvernement annonce un état d'urgences le 18 mars 2020, j'ai perdu mon travail par la suite.
Un arrêt total de toutes les activités commerciales, une interdiction global de voyage. tout un tas de mesures qui ont bloqués les activités de plusieurs commerçants, en plus la crise a durée tres longtemps du coup des faillites ont ètè annoncées, des pertes d'emplois, des suicides .... le seule point positif dans l'histoire est que je suis restée vivante du coup j'ai mis en suspension mes projets commerciaux.

## Challenge

Dans les fablabs, j'ai suivi une formation continue pour maitriser les outils fablab, par la suite j'ai suivi toute une série de procedure pédagogique encadrée par notre professeur pour arriver a atteindre le but du cours.
Commençant par penser a notre souci actuel, j'ai directement pensé a mon commerce fermé. la question est comment je pourrais developper une activité commerciale, trouver un chalenge, s'alligner avec les techniques durable et utiliser les techniques de fablab.

J'ai reçu par la suite des remarques de mes camarades pour m'aider a developper l'idée

![](../images/question.jpg)

les questions peuvent être regrouper sous les grandes lignes ci-dessous

1. Comment determiner le type d'activité et elle répond a quel besoin?
2. Comment determiner les contraintes, les outils ..... ?
3. Comment faire face au différents changement des mesures sanitaire et rentrer dans les categories essentiels ?
4. Le commerce en ligne est il la seule possibilité ?
5. Comment combiner les elements de l'activité en pandémie avec le marché après-pandémie   

j'ai fait des recherches concernant le développement des entreprises au milieu de la pandémie et les solutions adoptées ètait de (3)(4)(5):

1. Prendre le virage numérique
2. Diversifier les produits et les chaînes d’approvisionnement
3. Être agile

J'ai beaucoup réflechi a tous ses points et sur ce j'ai regardé ce que je pourrais réaliser localement au fablab avec les techniques de fablab et les produits de fablab.

## Concept

Dans le domaine de bijoux et accessoires mon idée de base ètait de chercher des bijoux anciens provenant du Maroc, les transporter içi et les remettre en tendance.

![](../images/bijouxberbere.jpg)

L'idée aujourd'hui est beaucoup plus interessante, il s'agit de la rècuperation des déchets fablab et les utiliser pour la création de bijoux, accessoires... la récuperation consiste a prolonger la durée d'utilisation des déchets.

![](../images/recyclag.jpg)

![](../images/bijoux.jpg)

J'ai pensé a faire appel a tous les ètudiants ULB/VUB en premier ou créateurs professionels/amateurs, l'idée est de crèer de nouvelles oportunités de travail. J'achète leurs créations, d'autres ètudiants ou chercheurs formés aux machines peuvent les fabriqués et je les mettrai en vente en ligne.

Ce travail peut ètre aussi integrés au sein des activités scolaires liées a la création.

## Objectifs de développement durable

![](../images/sust.jpg)

Ce projet soutient les objectifs de développement durable des nations unies(6). celui-ci vise a façonner le progrés économique mondial d'une manière socialement juste et dans le respect des limites écologiques de la terre.

![](../images/01.jpg)

Le premier goal est de mettre fin à la pauvreté sous toutes ses formes, il a ètè déclaré que les jeunes travailleurs sont deux fois plus susceptibles de vivre dans l'extrême pauvreté que les travailleurs adultes
mon projet crèe des nouvelles opportunités de travail aux jeunes

![](../images/05.jpg)

Le deuxieme goal est de réaliser l'égalité des sexes et l'autonomisation de toutes les femmes et filles, mon projet couvre aussi le travail a tous les sexes et trés demandé par les femmes.

![](../images/08.jpg)

Le troisieme goal est de promouvoir une croissance économique soutenue, inclusive et durable, le plein emploi productif et un travail décent pour tous. surtout apres la crise covid19 des millions de personnes ont perdu leurs emplois.

![](../images/09.jpg)

Promouvoir une industrialisation inclusive et durable et favoriser l'innovation

![](../images/12.jpg)

Assurer des modes de consommation et de production durables, en recupérant les déchets des fablabs le projet prolonge la durèe de vie de ses déchets et construit un avenir plus durable


## Etude scientifique

Trouver le bon materiau compatible a la création des accessoires et compatible a l'utilisation dans les machines fablab, en étudiant les materiaux possible et en regardant dans les déchets. j'ai trouvé plusieurs possiblités, **il est important avant utilisation de connaitre le materiau, ses caracteristiques, sa reaction au rayons lasers, et sa compatibilté a l'utilisisation avec les machines**.

Voici quelques méthodes simples d'identification des materiaux:

l'identification des materiaux peut se pratiquer dans des laboratoires équipés pour determiner la nature, ceçi est pour avoir des informations géneral et une identification façile. se poser la question ce dechet à quelle classe de materiaux s'agit il, métal, polymère ou ceramique? s'agit il d'un matériau composite ?
ci-dessous un tableau qui regroupe les propriété physiques de ses 3 classes

![](../images/tableaucara.jpg)

Pour les materiaux mettaliques, cinque mesures apportent un indice important:

1- Mesurer la densité, en calculant le rapport masse/masse d'eau. cette derniere est detecté via la méthode d'immersion de l'objet dans l'eau.

2- Detecter la conductivité éléctrique, si il est conducteur de l'èlectricité a l'aide d'un contrôleur en fonction ohmmètre.

3- Tester l'attirance par un aimant, cette technique montre qu'il s'agit d'un materiau ferromagnètique.

4- Mesurer le potentiel de corrosion, il s'agit de mesurer dans l'eau salée la difference de potentiel entre le materiau et une électrode de réference (électrode électrochimique au sulfate d'argent ou au calomel). cette technique identifie facilement les aciers, les alliages de cuivre et de zinc.

5_ Detecter les ètincelles en meulant a sec, il s'agit de differencier les aciers en fonction de leur teneur en carbone, les fontes, les aciers inoxydables et les alliages de titane.

Pour les materiaux polyméres, les propriétés physiques sont trés voisines, les fabriquants et selon la réglementation européenne apposent sur chaque pieces un sigle indiquant la famille de polymére, afin de faciliter le tri et le recyclage ci-dessous les logos et leurs familles correspondantes

![](../images/poly.jpg)

Dans le cas ou aucune identification n'est repérée ou a ètè masquée, l'identification peut se faire via les tests suivants qui fait le tri entre les trois ètats des plyméres: élastoméres, thermodurcissables ou thermoplastiques.

![](../images/organigrame.jpg)

1- Test de déformation. C'est un élastomère si on obtient une grande déformation élastique (avec retour constaté à la longueur initiale) en tirant à la main.

2- Test de chauffage. L'échantillon est chauffé par contact avec une pièce très chaude (verre ou métal). S'il ne fond pas et ne se déforme pas, c'est un thermodurcissable.

3- Test de densité. Seules les polyoléfines (P-éthylène et P-propylène) flottent sur l'eau.

4- Test de Beilstein. Détection de chlore dans la molécule. On chauffe un fil de cuivre enrobé du polymère dans une flamme. C'est un PVC si on obtient une flamme verte.

5- Test du solvant. On immerge un fragment du polymère pendant 10 minutes dans de l'acétone. Puis on verse un peu d'eau. Le test est positif si on observe un précipité.

6- Test du papier pH. Chauffer jusqu'à pyrolyse un fragment de polymère dans un tube à essais en maintenant un morceau de papier Ph humidifié à l'entrée du tube. Si les fumées et vapeurs sont basiques, on dégage une amine ou une amide. Si les fumées sont acides, c'est un acide gazeux (HCl, HCN, HF ou SO2).

7- Test de combustion. Si la combustion est facile, avec une flamme claire, le test est positif (AF et SF signifient avec et sans fumée). Si l'échantillon se recouvre de poudre blanche réfractaire (SiO2), c'est un silicone.

Cette méthode peut être fortement faussée si le matériau contient plusieurs phases : s'il s'agit de polymères armés, s'il s'agit de mousses, et dans le cas de certains copolymères (polyoléfine + autre).


Parmis les déchets, une palette des panneaux jetés au rez de chaussée, ah non ces panneaux sont partout.

![](../images/palette.jpg)

![](../images/boite.jpg)

Grace au marquage sur le materiau, il a ètè detecté facilement qu'il s'agit des PLEXIGLAS PMMA de formule chimique (C5H8O2)n, Le polyméthacrylate de méthyle est un thermoplastique transparent dont le monomère est le méthacrylate de méthyle (MAM). Ce polymère est plus connu sous son premier nom commercial de Plexiglas(1).

![](../images/formule.jpg)

Il est caracterisé par une très grande transparence, très limpide avec une excellente tenue aux rayons ultraviolets et à la corrosion, une légèreté sa masse volumique de 1,19 g cm−3 (beaucoup plus léger que le verre), un bon coefficient de dilatation thermique de 70−77 × 10−6 K−1 et une faible conductivité thermique à 23 °C de 0,17−0,19 W m−1 K−1
une résistance médiocre aux basses températures, à la fatigue et aux solvants. (2)

Ce n'est qu'à partir de 200 degrés Celsius environ que le matériau commence à fusionner et il ne dégage aucun gaz à toxicité aiguë(7).

Le PLEXIGLAS présente une bonne résistance aux substances inorganiques, aux acides et aux solutions alcalines à faible concentration, aux sels et aux solutions salines.
Par contre il ne résiste pas aux composés organiques, aux hydrocarbures chlorés, aux cétones et aux esters.
Grâce à sa résistance chimique, le PLEXIGLAS peut être nettoyé facilement et résiste aux produits de nettoyage du commerce. Cependant, les agents contenant du benzène, de l'éthanol et d'autres alcools, des matières organiques ou des diluants ne conviennent pas(7).

et pour finir il a ètè classé dans le groupe 3, inclassable quand à sa cancérogénicité par le Centre international de recherche sur le cancer (CIRC)(8).

Les lasers CO2 sont formidables pour la découpe et la gravure laser de ses panneaux. Ils offrent les conditions optimales pour la production d’objets de décoration, de jouets, et bien plus encore.

![](../images/objets.jpg)

Pour rappel ci-dessous une liste des materiaux qui ne peuvent étre utiliser avec les lasercutters

Les matériaux réfléchissant : miroirs, objets chromés, métaux polis, …

La plupart des métaux, le mode découpe peut cependant être utilisé pour marquer certains métaux

Fibre de verre

Carte de circuit imprimé (Fibre de verre + époxy)

Fibre de carbone

Tous les matériaux contenant du chlore, PVC, vinyl, … - Risque d’émission de gaz chloré mortel

Tous les matériaux contenant du fluor : Téflon / Polytétrafluoroéthylène / PTFE… - Risque d’émission de fluor sous forme de gaz

Verre

Les matériaux réfléchissants (miroir, métaux polis, chrome, …) L'utilisation de matériaux réfléchissants pourrait endommager fortement la machine

Medium valcromat teinté dans la masse - Prend feu

Polycarbonate / PC / Lexan / Makrolon : fond et brûle

Polystyrène expansé/extrudé (mousse): fond et brûle

Les matériaux composés et/ou non homogènes sont généralement compliqués à découper proprement au laser

et ci-dessous une liste des materiaux qui peuvent étre utilisés avec les lasercutters:

Bois

Bois brut (faible épaisseurs)

MDF / Medium (éviter les épaisseur >6mm qui on un rendu très brûlé et émettent beaucoup de fumée)- Ne pas utiliser de MDF tinté dans la masse

Contreplaqués

Certaines matières plastiques :

Polyamide / PA / Nylon

Polyoxyméthylène / POM / Delrin

Polyester / PES / Thermolite / Polarguard

Polyéthylène téréphtalate / PET / Mylar

Polyimide / PI / Kapton

Polystyrène / PS

Acrylique / Polyméthylmétacrylate / PMMA / Plexiglas

Polypropylène / PP

Acrylonitrile-butadiène-styrène / ABS

Rhodoïd / Transparent pour rétroprojecteur

Mousses :

Polyester / PES

Polyéthylène / PE

Polyuréthane / PUR

Neopren - Prend feu facilement

Tissus (feutre, chanvre, coton, acrylique, nylon)

Cuir

Papier

Carton, carton bois

Carton plume (carton+mousse PU, disponible sous les marques Canson et Kapa) - Attention : Le carton mousse classique (carton+Polystyrène expansé) n'est pas autorisé dans la laser, à cause de polystrène (brûle et fond).

Caoutchoucs naturel, synthétique (uniquement s'ils ne contient pas de chlore) - Attention génère beaucoup de suie et encrasse énormément les machines.

Le principal inconvénient du plexiglas réside dans son prix, ce n'est pas le cas nous l'aurons gratuitement via les fablabs ou nous pourrons le recuperer via les autres fablabs. Il faut aussi noter que le plexiglas a tendance à se rayer, nous pourrons protéger la surface via un vernis ou des accessoires de décoration


**Geometrie:**

Un test de realiser plusieurs geometries pour detecter le comportement de materiau, il a ètè conclue que l'épaisseur ideal est pas moins de 1mm, une épaisseur de 0,5 mm ça devient trés fragile et ça casse.
en plus il faudrait éviter les bords pointues et les remplacer par des bords arrondis.

sinon le materiau répond super bien a la découpe, des bons resultats ont ètè obtenues.

## Realisation

J'ai choisi la machine Full Spectrum Muse pour sa disponibilité et sa facilité, les autres machines laser sont aussi compatibles. Il s'agit d'une machine controlé a distance via l'interface Retina Engrave.

![](../images/muse.jpg)

les specifications de la machine sont:

Surface de découpe : 50 x 30 cm
Hauteur maximum : 6 cm
Puissance du LASER : 40 W
Type de LASER : Tube CO2 (infrarouge) + pointeur rouge

Pour y acceder, il faudrait connecté le pc a la machine par le retour de wifi: lasercutter MDP : fablabULB2019 et acceder a l'adresse suivante [URL](http://fsl.local )

Pour apprendre à utiliser Retina Engrave, suivez les [tutoriels vidéo](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6)

Formats de fichier pris en charge:

Vectoriel : SVG, Le fichier sera importé en vectoriel mais aussi en matriciel, il faudra donc en supprimer un des deux.

Matriciel : BMP, JPEG, PNG, TIFF

Avant tout, il est important de connaitre son materiau et l'effet du laser en fonction de l'intensité et de la vitesse de passage sur le materiau.
Pour cela, il est courant de réaliser des essais de calibration.

![plaque de calibration](../images/plaquecalibration.jpg)

Pour toutes les realisations j'ai utilisé Inskape

Avant de lancer toutes les découpes, j'ai entamé des checks du plan de travail avec le carreau en haut a gauche avec cette astuce la machine commence a encadrer le dessin, en regardons dans la machine nous pourrons checker si le materiau est bien positionné. et finalement nous lançons la découpe.

Apres avoir detecter le calibrage nous pourrons ensuite découper de la meme maniere d'autre dessin, géneralement j'ai utilisé 80 comme power et 20 comme Speed et pour graver j'ai utilisé 50 de power et 40 de speed.

L'interface de communication possède un ècran oû nous pouvons encore modifier notre dessin, et encore il a un point rouge qui montre la tete du faisceau laser nous pourrons controler cette position via cette interface pour pointer au bord du dessin, nous pourrons faire de meme dans la machine placer notre dechet sous le faisceau.

veuillez trouver ci-dessous des photos de mes creations

![](../images/inkscapearbre.jpg)

![](../images/realisationarbre.jpg)

![](../images/realisation.jpg)

![](../images/papillons.jpg)

![](../images/lettres.jpg)

![](../images/bijouxfinaux.jpg)

## Optimisation

Pour des raisons de simplification j'ai choisi d'utiliser la machine laser cutter Full Spectrum Muse vu que je pourrais dessiner directement sur mon pc, facile a utiliser moins de bruit connecté par connexion wifi et compatible a des petits dessin.

A la fin je me suis rendu compte que avec l'autre machine Epilog Fusion Pro 32 vu qu'elle posséde d'une camera nous pourrons placer facilement les dessins sur le plan de travail sans penser deux fois.

Il faudrait optimiser la surface et récuperer le maximum possible du materiaux de la manière la plus efficace possible, et aussi gagner du temps. [Deepnest.io](https://deepnest.io) est un logiciel de Nesting open source, idéal pour les découpeurs laser, les découpeurs plasma et autres machines CNC. il permet de reorganiser et compacter les dessins sur une surface particulière.
Il peut lire des fichiers DXF, SVG, Corel CDR et exporte des fichiers DXF et SVG. il existe un autre logiciel pareil il s'appelle [svgnest](https://svgnest.com).

Dans mon cas je vais essayer de reorganiser les creations sur les plaques de déchets, ses plaques portent la meme forme, j'ai demandé la récuperation des fichiers SVG de base vu que ses formes ont ètè realisées dans les fablabs, j'ai pu rècuperer facilement le fichier, il comporte le dessin complet de la plaque.

![](../images/plaquedechetcomplet.jpg)

J'ai modifié sur inkscape le dessin pour avoir la mini plaque, il faudrait ajouter un fond pour avoir une surface lisible sur Deepnest, je suis passée par crèer un chemin qui encadre les surfaces puis Exclusion des masques.

![](../images/plaqueinkscape.jpg)

Apres avoir télecharger le logiciel qui est compatible a mac, windows et linux il faudrait importer les fichiers .svg sur deepnest, la mini-plaque ainsi que les autres dessins de bijoux a nester.
Par la suite il faudrait modifier les parametres, verifier que l'échelle est bien de 90 units/inch si vous utilisez inskape et 72 units/inch pour illustrator.
j'ai fixé l'espace entre les elements a 4mm de securité et surtout contre le bord.
par rapport au nombre de rotation souhaité, comme j'ai une forme irreguliere j'ai choisi un nombre élevé 8 de rotation mais un 4 est suffisant pour des formes comme les rectangles.
Il faudrait choisir le type d'optimisation souhaité, le choix s'impose entre gravity, bounding box & squeeze. ce dernier est le plus adapté a des formes irregulier et il permet l'optimisatio sur toute la surface tandis que les autres sont plus utilisable pour des surface regulieres et tend a reduire la surface consomé en commançons a nester par un coin ou un coté. par rapport au rapport d'optimisation 0 c'est quand nous souhaiterons avoir une optimisation de materiau et 1 pour l'optimisation de temps et du materiau, j'ai pris la valeur par defaut 0,5.
pour finir n'oubliez pas de passer de inch au mm pour avoir les dimensions exactes des differents élements.

![](../images/deepnestconfig.jpg)

Apres avoir importer les fichiers choisir la quantité des élements a nester et lancer le logiciel.

![](../images/nesté.jpg)

Il va établir plusieurs combinaisons, elles seront classées une derriere l'autre vous pouver choisir parmis ce que vous preferez mais la meilleure géneralement et la dérniere combinaison. j'exporte le fichier .svg et je le lance vers la machine laser.

![](../images/bijouxnesté.jpg)

Je suis passée par ma preferée Full Spectrum Muse, du coup j'ai importé aussi le dessin de la mini plaque pour pouvoir les placer correctement sur la surface apres un test d'encadrage je lance la découpe, vous pouvez éviter le test facilement en utilisant la Epilog Fusion Pro 32 muni de sa camera.

![](../images/bijouxnestélaser.jpg)

![](../images/bijouxnestélaserfinale.jpg)

l'essai sur la plaque complete, mais la machine étais desinstallé en tout cas sur inskape j'ai pu obtenir la surface exterieurs voir ci-dessous

![](../images/plaquecomplete.jpg)

Et le fichier nesté ressemble a ça, le logiciel a reussit a reorganiser 250 pieces dans la plaque. Il ne reste plus que lancer la découpe

![](../images/plaquecompletenesté.jpg)

Pour finir il est utile d'investir a avoir un scanner pour surface, il permet d'avoir le plan de n'importe quel surfaces ou materiaux industriels

## Creation d'entreprise, creation de site WEB.

Avant de lancer tout projet, une ètude de marché doivent être réaliser. Première étape est de définir le marché, sur quel marché l'entreprise va-t-elle évoluer
qui seront les clients ou les utilisateurs, quelle est la dimension géographique du ou des marchés que vous souhaitez cibler, quelles sont les évolutions du marché en valeur et en volume. Detecter les produits concurents et les acteurs.

Deuxième étape est d'analyser la demande, evolution globale de la demande, comportement du client et de l'utilisateur, segmentation de la demande.

Troisième étape est l'analyse de l'offre, evolution globale de l'offre, caractéristique de l'offre et des entreprises concurrentes.

Quatrième étape est l'analyse de l'environnement du projet au niveau politique, economique, sociale, technologique, écologique et légale.

c'est ce qu'on appelle ètablir un business plan, j'ai deja cherché les informations necessaires mais cette ètude va ètre ètablie apres la validation du projet.

Afin de déposer une marque il faudrait savoir si le nom et le logo sont disponible, Avant de déposer un signe et de commencer les procédures qui ont un certain coût, il est conseillé de vérifier d'abord si le signe choisi satisfait bien aux conditions de protection.

Vérifiez également que le signe est toujours disponible pour les produits ou services que vous avez en vue. Pour cela, vous pouvez consulter gratuitement le registre des marques en ligne de l'Office Benelux de la Propriété Intellectuelle, qui contient non seulement les marques Benelux, mais également les marques de l’Union européenne et internationales. [BOIP Registre de marques](https://www.boip.int/fr/registre-des-marques#/).

Le magasin va porter le nom de M.G. Designs, pour gérez et développez le magasin en ligne, avoir une vitrine personnalisée, de canaux de vente intégrés, une gestion simplifiée des catalogues, commandes, frais d'expédition, taxes...
il est nécessaire de créer un site web. les leaders sur le marché sont Shopify, Wix, Ecwid, bigcartel, Squarespace et wordpress.

le plus façile a utiliser et avec une gratuité d'essai illimité est wix du coup j'ai developpé un sitweb sur wix.

![](../images/sitewebmgdesigns.jpg)

Pour une heure de travail, nous pourrons découper 60 élements, deux possibilités pour payer les collaborateurs seront envisager, payer au pourcentage de vente comme ça je pousse chaque créateurs et fabriquateurs a m'aider au marketing et le réseau des clients sera plus grands. sinon j'achète les dessins de creation, je paye des ètudiants pour les découpes et finition et enfin je m'occuperai de la vente en ligne. une ètude budgetaire plus detaillèe sera muni afin de prendre la decision la plus efficace.

l'étape suivante est de lancer une video sur la plate forme Tiktok pour voir les reactions des internautes.

Souhaitez moi bonne chance dans cette aventure, et n'oubliez pas de me soutenir.
Je remercie tout le monde pour cette chouette experiance et pour tous ce que j'ai appris au fablabs.

### References

(1) Nom et abréviation selon la norme EN ISO 1043-1, Plastiques - Symboles et termes abrégés - Partie 1 : polymères de base et leurs caractéristiques spéciales.

(2) http://www.goodfellow.com/F/Polymethacrylate-de-methyle.html

(3) https://www.bdc.ca/fr/articles-outils/operations/acheter/covid-19-etapes-gerer-risques-chaine-approvisionnement

(4) https://www.bdc.ca/fr/articles-outils/marketing-ventes-exportation/ventes/comment-etablir-rapidement-votre-presence-enligne-pour-generer-revenus-periode-crise

(5) https://www.bdc.ca/fr/articles-outils/blogue/covid19-rendre-votre-entreprise-resiliente-contexte-pandemie

(6) https://sdgs.un.org/goals

(7) https://www.plexiglas.de/en/service/product-info/unique-properties

(8) IARC Working Group on the Evaluation of Carcinogenic Risks to Humans, « Évaluations Globales de la Cancérogénicité pour l'Homme, Groupe 3 : Inclassables quant à leur cancérogénicité pour l'Homme » [archive], sur monographs.iarc.fr, CIRC, 16 janvier 2009 (consulté le 22 août 2009).

(9) http://stockage.univ-valenciennes.fr/EcoPEM/BoiteA/co/A_1_C.html

## Useful links

- [How to Use Deepnest for Lasercutting](https://www.youtube.com/watch?v=CnFDfcMmazA)
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
