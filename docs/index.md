## About me

![](images/avatar-photo.jpg)

Hello
Je m'appelle _Majda Ghalem_ je suis ingénieur de construction, je poursuis mes études a l'[ULB](https://www.ulb.be) en fusion avec la [VUB](https://www.vub.be).
j'aime bien les challenges et me lancer sans limite, la raison de mon choix de découvrir les FABLABs.

## Previous work

J'ai travaillé dans la construction pendant 3 ans et j'ai fait plusieurs chantiers de nouvelles construction et des renovations.​


## Projects

ci-dessous des images de premiers chantiers que j'ai réalisé avec ma team

# _Clinique sans souci_
![](images/clinique.png)
# _Station métro Simonis_
![](images/sample-photo.jpg)
# _Projet Wiertz_
![](images/wiertz.jpg)
