# 6. Electronique - Fabrication

Cette seance je vais apprendre a comment creer my arduino board. et donc Fabriquer une carte de développement à microcontrôleur, pour prototyper des applications électroniques.

## Les ètapes suivies:

Nous avons reçu une Carte de circuit imprimé, elle a été conçue pour simplifier le circuit au maximum avec des composants faciles à souder.
La carte va étre designer comme ceci :
Un microcontroleur [Atmel SAMD11C](https://www.microchip.com/wwwproducts/en/ATSAMD11C14) comme noyau
Une LED POWER (rouge) : toujours allumée, connectée à l'alimentation.
Une LED BUILTIN (verte) connectée au PIN 15
Un BOUTON connecté à la broche 8
Une broche protégée par 5V vers la broche 5, qui est aussi la broche RX du SERIAL matériel (~UART).
Le bootloader a déjà été programmé dans la puce pour faciliter le travail.
la programmation de la carte via ArduinoIDE grâce au SAMD CORE pour arduino, développé par MattairTech.

### les soudures

Concernant la carte, Il faudrai verifier S'il reste du cuivre à l'avant du connecteur USB, retirez-le (avec un cutter par exemple).
Soudure des différents elements, pour créer un point de soudure on utilise un fils métallique en étain que nous chauffons avec un fer a Souder, un bon point de soudure doit être lisse et brillant.

1.Souder le microcontroleur:
Verifier les directions: repérer le point, le petit point sur le micro-contrôleur doit être placé en haut à gauche lorsque la carte est orientée avec le connecteur USB en bas.
Verifier l'allignement des pattes.
Fixez le en soudant deux pattes opposées mettez une petite quantité de soudure sur un pad, soudez une patte en bas, puis soudez la patte opposée en haut.

1er essai raté: au lieu de fixer les pattes sur le cuivre je l'ai fixé sur le circuit, je devais l'enlever sauf que je l'ai arraché donc je me retrouve avec un microcontroleur sans patte et le cuivre arraché :s

![](../images/carte.jpg)

Pour réparer le cuivre arraché dans la carte on a soudé directement sur toute la longueur pour le fixer mais on a du changé le microcontroleur.
Résultat, avec beaucoup de difficulté j'ai réussi a souder les pattes veuillez voir la photo ci-dessous

![](../images/carte1.jpg)

2.Souder le regulateur
3.souder le Condensateur 3.3V
4.sur l'autre face, j'ai mis un petit morceau de ruban adhésif et Coller avec le petit adaptateur plastique pour le connecteur USB
resultat:

![](../images/carte2.jpg)

![](../images/cartex2.jpg)

Il faudrait checker si tout fonctionne correctement, il faudrait brancher la carte dans le pc comme j'ai un mac j'ai checker dans le pc de nicolas, on a eu un beug dû a un court circuit comme la carte a chauffé. c'est au niveau du condensateur que l'étain a touché au moment de la soudure les deux coté du cuivre le + et le - donc on a réparé sa et puis la carte a fonctionné correctement.
pour verifier sur mon pc encore j'ai ouvert arduino et dans outils/ port, on va avoir l'acces a la nouvelle carte branchée.

5.Soudez la LED (verte) et sa résistance. Vérifiez la polarité avant de souder en utilisant un multimètre en mode contrôle de continuité ou le repère sur la LED (noir dans la masse)
resultat la carte est prête a utilisation:

![](../images/carte3.jpg)

#### Configuration Arduino

Passer a l'étape de Configurez Arduino pour pouvoir utiliser SAMD11C
Dans Preferences ajoutez l'url de gestionnaire de carte supplémentaire

```
https://www.mattairtech.com/software/arduino/package_MattairTech_index.json
```

Puis dans Arduino, selectionez :
Board : Generic D11C14A
Clock : INTERNAL_USB_CALIBRATED_OSCILLATOR
USB-CONFIG : CDC_ONLY (or any if you're sure about what you're doing...)
SERIAL_CONFIG : ONE_UART_ONE_WIRE_NO_SPI
BOOTLOADER : 4kB Bootloader
gardez les autres parametres par defaut

Afin d'essayer le bon fonctionnement j'ai lancé le programme Blink qui a de bute allumer et eteindre la LED toutes les secondes, il faudrait faire quelque addaptation dans le code ardhuino, il faudrait changer les entrèes avec les nouvelles entrèes de la nouvelle carte. le tableau a suivre et ci-dessous

![](../images/tableau.jpg)

voila tout fonctionne correctement, voici une video qui montre cela:

![](../video/carte.mp4)

Apres avoir rassurer que tout fonctionne correctement on peut continuer les soudures.

6.Soudez tout le reste, en commençant par la résistance de protection (à côté du régulateur de tension). la LED rouge, Puis le bouton et enfin les .
Voici la carte finale

![](../images/carte4.jpg)

Finallement, je connecte encore une fois la carte pour la tester, la LED rouge s'allume correctement.

je vais lancer un programme pour tester le fonctionnement mais avant tout il faut brancher la carte avec le circuit que j'ai realisé la semaine passé.

![](../images/carte5.jpg)

Je recois un message, la capacité de la carte ne prend pas en charge un code assez dense. je tente un nouveau code plus simplifier et  cette fois ci je decide de réaliser un circuit standard d'un lED que je connnecte directement a ma carte et voila elle s'allume correctement. voir la photo ci-dessous

![](../images/carte6.jpg)


## Useful links

- [Jekyll](http://jekyll.org)
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
