# 4. Découpe assistée par ordinateur

Cette seance j'ai fait la découverte de plusieurs machine de découpe au laser: Epilog Fusion Pro 32, Lasersaur & Full Spectrum Muse et une découpeuse vinyle.

Instruction a connaitre avant toute utilisation:

Connaître avec certitude quel matériau est découpé, il faudrait Toujours activer l’air comprimé, allumer l’extracteur de fumée, Savoir où est le bouton d’arrêt d’urgence.

en cas de besoin, Savoir où trouver un extincteur au CO2

Rester à proximité de la machine jusqu'à la fin de la découpe.

## Full Spectrum Muse

Nous avons utilisé cette machine pour notre essai, il s'agit d'une machine controlé a distance via l'interface Retina Engrave.
les specifications de la machine sont:

Surface de découpe : 50 x 30 cm
Hauteur maximum : 6 cm
Puissance du LASER : 40 W
Type de LASER : Tube CO2 (infrarouge) + pointeur rouge

Pour y acceder, il faudrait connecté le pc a la machine par le retour de wifi: lasercutter MDP : fablabULB2019 et acceder a l'adresse suivante [URL](http://fsl.local )
Pour apprendre à utiliser Retina Engrave, suivez les [tutoriels vidéo](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6)

Formats de fichier pris en charge:

Vectoriel : SVG, Le fichier sera importé en vectoriel mais aussi en matriciel, il faudra donc en supprimer un des deux.

Matriciel : BMP, JPEG, PNG, TIFF

### Calibration

Avant tout, il est important de connaitre son materiau et l'effet du laser en fonction de l'intensité et de la vitesse de passage sur le materiau.
Pour cela, il est courant de réaliser des plaques de calibration, sur bois, carton et nous avons effectués Simone, Paul et moi celle sur papier.

![3 plaques de calibration pour découpeuse laser : sur bois, carton et papier](../images/calibrationSheet.jpg "plaques de calibration")

Pour realiser cela, Nous avons dessiner directement sur Inskape des carreaux puis on a configuré les traits avec des couleurs differentes et des parametres de Speed et power differents.

Avant de lancer la découpe, nous avons entamer un check du plan de travail avec le carreau en haut a gauche avec cette astuce la machine commence a encadrer le dessin, en regardons dans la machine nous pourrons checker si le papier est bien positionné. et finalement nous lançons la découpe.
vous trouverez le fichier disponible [ici](../files/laser/CalibrationPapermuse export.re3)

![](../images/calibrationPaperMuseLaserCutter.jpg)

Apres avoir detecter le calibrage nous pourrons ensuite découper de la meme maniere d'autre dessin en modifiant les parametres voici quelques exemples realisés prises du lien suivant [URL](https://fablab-ulb.gitlab.io/projects/2020-fabxlive/workshop-mechanical-metamaterials/stretchable/):

Kirigami

![](../images/squareKirigami.jpg)

![](../images/example2.jpeg)

![](../images/example1.jpeg)

## Découpeuse vinyle

Pour la decoupeuse vinyle, je vais utiliser la Silhouette Cameo, il faudrait installer Le logiciel pour se connecter avec la machines c'est le Silhouette Studio. Une version gratuite est disponible [ici](https://www.silhouetteamerica.com/software)

Dans la boutique en ligne vous trouverez des modeles gratuits vous pouvez ajouter des texts et des dessins directement dans l'outil design ou modifier pour simplifier c'est ce que j'ai fait.
dans envoyer, j'ai precisé pour chaque ligne a couper le materiau, la lame, force et vitesse. il faudrait lancer un test avant d'envoyer afin d'être sur des parametres choisis que ça correspond bien.
le fichier est disponible [ici](../files/studio/dragon.studio3)

![](../images/studio.jpg)

le resultat finale ressemble a ça.

![](../images/studio2.jpg)
