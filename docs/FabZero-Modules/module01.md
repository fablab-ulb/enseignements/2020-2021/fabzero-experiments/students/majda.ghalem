# 1. Documentation


# Module 1 : INSTALLATION GIT

Dans le but d'apprendre a travailler sur Gitlab, cette semaine nous avons fait la découverte de quelques notions de base de codage nécessaires pour l’utilisation de GitLab afin de faciliter le partage de nos connaissances.

Le but est de laisser des traces pour chaque étape et chaque erreur pour aider les autres a avancer. Je vais vous détailler mes avancements au fur et a mesure, enfin je vais essayer.

## CONFIGURATION GITLAB

Rediger du texte

Vous pouvez écrire directement via la plateforme de [GitLab](gitlab.com), puisque celle-ci modifie automatiquement le dossier écrit grâce à [Mkdocs](https://www.mkdocs.org).
Pour rédiger mes documentations, ainsi que ma presentation j'ai utilisé le logiciel [ATOM](https://atom.io) il faudrait aussi installer le package [markdown-preview-enhanced](https://atom.io/packages/markdown-preview-enhanced).
Pour ajouter des photos, il faudrait changer avant tout leurs dimensions vers un 600*600 pour cela, j'ai utilisé le logiciel [GIMP](https://www.gimp.org/downloads/) et les encoder comme ceci "![] (../images/photo.jpg)"
pour s'entrainer a comment écrire sur ATOM je vous conseille de s'exercer avec le lien que le professeur a envoyé [markdown_starter](https://www.markdowntutorial.com/)

Pour le transfert vers le serveur il faudrait telecharger [Git](https://git-scm.com). il s’agit d’un logiciel de gestion utilisé pour gérer de la documentation et le travail de groupe.
Il est nécessaire d’installer Shell Bash pour faire tourner ça, je l'ai par défaut sur mon ordinateur Mac. il suffit d'appeler Terminal dans votre barre de recherche.
par la suite j'ai demandé a mon ordinateur pour savoir si Git est installé sur mon ordinateur, avec la commande git --version voir la photo ci-dessous.

![](../images/gitversion.jpg)

Parfait il est bien installé, pour le configurer il faudrait s’identifier directement via le terminal avec son nom et son e-mail (voir photo ci-dessous). les identifications doivent être identique a ceux de Gitlab.
Definissez le user_name et l'adresse email comme sur la photo.
 ![](../images/gidentification.jpg)

Pour verifier la configuration executez git config --global --list le terminal répondra pour vous confirmer l'identification (voir la photo ci-dessous)
 ![](../images/gitconfig.jpg)


a n'importe quel moment le terminal ne répond plus, faudrait le fermer et le relancer ça m'a arrivé une fois

Pour connecter mon ordinateur à GitLab, il faudrait ajouter des informations de sécurité pour m’authentifier, via une clé SSH.
Pour créer une clé SSH ED25519, executez la commande
ssh-keygen -t ed25519 -C "<comment>"
pour plus d'information dur la clé SSH voir le lien suivant [SSH](https://docs.gitlab.com/ee/ssh/README.html#see-if-you-have-an-existing-ssh-key-pair)
felicitation vous avez crèez une nouvelle clés.

![](../images/ssh.jpg)

la clé est aussi enregistrée dans votre ordinateur, vous retrouvez tous ce qu'il faut dans un dossier nomé .ssh.
Dans ce dossier vous retrouvez une clé privée et une clé publique, il faudra travailler avec la publique notée .pub
je l'ai trouvé avec une grande difficulté, pour eviter cela j'ai executé la commande suivante pour copier la clé :

![](../images/copyssh.jpg)

Ensuite il faudrait naviguer a [GitLab](gitlab.com), en haut a droite selectionner votre avatar puis modifier, a gauche selectioner SSH Keys. coller la clé copiée auparavant puis ajouter.

![](../images/gitssh.jpg)

Pour s'assurer que l'authentification a reussi executez la commande: ssh -T git@gitlab.com
il faudrait cliquer entrer e n'ai pas vu que le terminal m'a posé une question dont je devais répondre *YES* et donc l'authentification a ètè echoué voir la photo ci-dessous

![](../images/gitauthentificationfailure.jpg)

pas de probleme j'ai refait la commande et j'ai bien repondu yes tout est en ordre ci-dessous la reponse du terminal.

![](../images/gitauthentification.jpg)


L’étape finale consiste à créer un lien spécifique entre GitLab et l'ordinateur. c'est de creer un dossier local correspondant a celui de GitLab, sur le compte GitLab, cliquez sur “clonage” Séléctionner la clé SSH et copier là.

![](../images/clone.jpg)

dans le terminal faite appel a l'emplacement du dossier local, cd /Users/majdamissette/Documents/fablab
executez la commande pour cloner les deux dossiers git clone + la clè trouvé

![](../images/gitclone.jpg)

et voila c'est pret pour etre utiliser, voir ci-dessous un essai qui consiste a utiliser le terminal pour apporter le dossier en ligne et le copier localement puis ajouter la presentation faite sur Atom appelé "test index" et l'envoyer au serveur Gitlab.

![](../images/essai.jpg)



## Useful links

- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
