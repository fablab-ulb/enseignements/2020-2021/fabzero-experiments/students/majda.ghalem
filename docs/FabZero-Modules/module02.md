# 2. Conception Assistée par Ordinateur

## Flexlinks

Dans cette seance on devait apprendre a dessiner avec openscad et freecad.
toutes les démarches sont bien expliqué dans la page de [Nicolas](http://fab.academany.org/2018/labs/fablabulb/students/nicolas-decoster/?page=assignment&assignment=02#OpenSCADInstall)

 j'ai essayé les tutos de base pour me familiariser avec les logiciels et je me suis retrouvé plus sur le openscad que le freecad étant donné que je n'ai pas de souris et la programmation sur openscad était plus facile pour moi.

Dans une premiere étape, j'ai consulté le site pour choisir un [flexlinks](https://www.compliantmechanisms.byu.edu/flexlinks), j'ai choisis le Fixed-Fixed Beam.
j'ai commencé avec le fixed-fixed beam Il est disponible [ici](../files/openscad/flexlinks.scad)

j'ai fais avec ce que j'ai trouvé sur le [cheet exemple](https://www.openscad.org/cheatsheet/) donc j'ai crèe une intersection entre un sphere et un cube avec le code suivant:

intersection(){cube(12, center=true); sphere(8);};

puis j'ai fait la difference avec un cylindre pour faire le trou, j'ai copié en 2 par la suite comme ceci:

```
for (i = [ 0 :  1]) {
translate([i*10,0,0])
difference (){
intersection(){cube(12, center=true); sphere(8);};
//creation de cylindre interieure
cylinder(h=12,r=4, center=true);}}

```

j'ai demandé de copier cet element en 2 fois puis j'ai crèe le beam de liaison

```
//creation de copie

for (i = [ 0 :  1]) {
translate([i*45,0,0])

//creation d'un corp avec 2 trous

for (i = [ 0 :  1]) {
translate([i*10,0,0])
difference (){
intersection(){cube(12, center=true); sphere(8);};

//creation de cylindre interieure

cylinder(h=12,r=4, center=true);}}}

//creation de la liaison

translate([28,0,0])
cube([25,2,6], center=true);
```

le resultat est dans la photo suivante:

![](../images/flexbeam.jpg)

##  Brick

Je me suis entrainé encore parceque je n'etais pas satisfaite du resultat et cette fois çi je voulais dessiner les bricks Il est disponible [ici](../files/openscad/piece.scad).
je suis partie par la creation d'un cube vide, par fair la difference de 2 cubes:

```
$fn=25;
difference(){
cube([31,15,9]);
    translate([1.45,1.45])
cube([28,12,8]);}
```

![](../images/cube.jpg)

j'ai demandé aussi de dessiner un cylindre et le copier dans les 2 directions x et y

```
translate([3,3])
for (j = [ 0 :  1]) {
for (i = [ 0 :  3]) {
translate([i*8,j*8,9])
cylinder(h=1,r=1.5);}}
```

![](../images/cylindresuperieur.jpg)

j'avais besoin de support a l'interieure donc j'ai crèe de cylindres vides pour faire une sorte de poutre puis les projeter au milieu comme ceci:

```
translate([7,7])
for (k = [ 0 :  2]) {
   translate([k*8,0])  
difference(){
cylinder(h=8,r=3);
cylinder(h=8,r=2);}}
```

![](../images/cylindreinterieur.jpg)

le resultat final avec une vue de dessous ressemble a la photo ci-dessous

![](../images/vudessous.jpg)
