# 5. Electronique 1 - Prototypage

Dans cette seance, j'ai appris à utiliser une carte de développement à microcontrôleur pour prototyper des applications électroniques en la programmant et en utilisant des périphériques d'entrée et de sortie.

Nous avons reçu une boite Kit pour les debutants qui contienne toute sorte d'outils et d'informations que vous aurez besoin.
Il faudrait télécharger le logiciel [Arduino](https://www.arduino.cc/en/software/). Pour des tutorials visitez le [Site](http://www.DFRobot.com)

Dans le logiciel vous allez trouver dans fichier/exemples, des exemples pour vous entrainez a encoder et connaitre des fonctions et dans le kit des fiches de projet.
j'ai commencé par réaliser un circuit électronique d'une lampe qui s'allume et s'éteint toutes les secondes.
ci-dessous le code, vous le trouverez aussi [içi](../Files/essailamp/essailamp.ino)

```
//Blink  Turns an LED on for one second, then off for one second, repeatedly.

const int LED=2;
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED as an output.
 pinMode(LED, OUTPUT);
 }

// the loop function runs over and over again forever
void loop() {
 digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
 digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
 delay(1000);                       // wait for a second
}

```

![](../images/lamp.jpg)

Dans le logiciel, n'oubliez pas de verifier les parametres de type de carte dans outils a Arduino Uno et le port Pour etre sur du port, le nom est affiché aussi en bas a droite

![](../images/outilsuno.jpg)

![](../images/outilsport.jpg)


Voici une video qui montre le fonctionnement de la lampe

![](../video/lamp.mp4)

Par la suite j'ai réalisé un circuit pour mesurer la temperature du lieu en utilisant un sensor toutes les valeurs relevées sont dans le moniteur serie en haut a droite
et par la suite j'ai ajouté un buzzer pour alerter a certaine temperature dans mon cas j'ai precisé une temperature <0 et >24 . vous trouverez le code [içi](../Files/temperaturealarm/temperaturealarm.ino)

```
/* Temperature Alarm */

#define ANALOGREFVOLTAGE 5.555

// TMP36 Pin
int temperaturePin = A0;

// Piezo Pin
int piezoPin = 8;
// Freezing
float freezeTemp = 0;
// Boiling
float boilTemp = 24
;

void setup()
{
  // Start the Serial connection
  Serial.begin(9600);
}

void loop()
{
   float temperature = 0;

   temperature = getVoltage(temperaturePin);


  // Convert to degrees C
  temperature = (temperature ) * 100;
  Serial.println(temperature);


  if(temperature < freezeTemp) {
  tone(piezoPin, 1100, 1000);
  }
  else if(temperature > boilTemp) {
  tone(piezoPin, 1100, 1000);
  }

  delay(1000);
}

float getVoltage(int pin) {

  return(float(analogRead(pin))* float(ANALOGREFVOLTAGE/1023.000));  
}
```

ci-dessous la temperature relevée et le circuit realisé.

![](../images/listetemp.jpg)

![](../images/alarm.jpg)

J'ai realisé une video qui montre le fonctionnement, pour voir j'ai chauffé le sensor a la main pour detecter une temperature >24 degres

![](../video/alarm.mp4)
