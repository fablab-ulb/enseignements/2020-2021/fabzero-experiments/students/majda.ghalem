# 3. Impression 3D

Cette seance, j'ai appris a dessiner en 3D, les fichies a imprimés doivent être convertis tout d'abord en fichier .stl

## Flexlinks

J'ai appris a utiliser une autre méthode de dessiner avec Openscad et je me suis basé sur le code trouvé chez [Stephanie](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/FabZero-Modules/module02/) pour dessiner le [half-circle](https://www.thingiverse.com/thing:3016943) et le combiner avec mes briques.

le half-circle est disponible [ici](../files/openscad/half-circlenew.scad)
Commençons par définir tous les paramètres que nous allons utilisés

```
$fn=50;
epsilon = 0.1;

//Define the connector variables

thickness_connector = 6.4; //in z direction
width_connector = 8; //in y direction
diameter_hole = 4.8;
distance_hole = 8;
length_connector = distance_hole+width_connector;//in x direction

//Define the beam variables

distance_connectors=50; //will fix the length of the curved beam
width_beam=1; //in the xy plan
thickness_beam=thickness_connector; //in z direction

//first connector

connector();

//second connector

translate([0,distance_connectors,0])connector();
```

Pour designer le connector on va demander la difference entre deux cylindres puis le translate afin de le bouger puis la fonction hull pour l'envelopper. a la fin on va le copier 2 fois

```
//connector design

module connector(){
difference() {hull(){
    translate([distance_hole,0,0]) cylinder(h=thickness_connector,d=width_connector,center=true);
    cylinder(h=thickness_connector,d=width_connector,center=true);};
    cylinder (h = thickness_connector, d=diameter_hole, center = true);
    translate ([distance_hole,0,0]) cylinder (h = thickness_connector, d=diameter_hole, center = true);}}
```

la photo ci-dessous nous montre les connectors

![](../images/connector.jpg)

pour dessiner le demi cercle qui relie les 2 connectors, on va tout d'abord demander la difference entre 2 cylindres qu'on va par la suite encore faire la difference avec un cube qui fait la longueur de la distance entre les deux connectors pour obtenir la liaison

```
    //the half-circle Beam
    translate([length_connector-width_connector/2-epsilon,distance_connectors/2,0]){
    difference(){
    difference(){
    //cylindre externe
    cylinder(h=thickness_beam,d=distance_connectors,center=true);
    //cylindre interne
    cylinder(h=thickness_beam,d=distance_connectors-width_beam,center=true);}
    //cube servant à couper
    translate([-distance_connectors/2,0,0]){
    cube([distance_connectors,distance_connectors, thickness_beam],center=true);}}}
```

ci-dessous nous avons le résultat final

![](../images/flexcircle.jpg)

## Dessin à imprimer

ci-dessous les fichiers .stl

[briques](../files/stl/piece.stl)

[half-circle](../files/stl/half-circlenew.stl)

## Impression 3D

Tout d'abord il faudrait prendre connaissance de la machine que nous avons a disposition et de ses caractéristique dans notre cas c'est Prusa I3MK3S.
Taille du plateau d’impression : 22,5cm x 22,5cm
Hauteur d’impression maximale : 25 cm
Diamètre de buse : 0,4mm
Il est important de connaitre le diamètre de la buse pour bien paramétrer la hauteur de couche des impressions.

Le logiciel [Slicer de Prusa](https://www.prusa3d.com/drivers/) va me generer un G-code. comment ?

une fois le logiciel installé il faudrait se mettre dans le mode expert, ouvrir le fichier .stl en le glissant directement dans le plateau.
il est important de bien orienter votre modèle par exemple choisir la base la plus large dans mon cas j'ai compris l'importance lors de l'impression des briquets que j'ai du refaire 3 fois parceque j'ai choisi la mauvaise surface d'impression il fallait les imprimer a l'envers vu que l'imprimante ne sait pas imprimer sur un vide et a besoin de support.

ci-dessous une photo de la bonne surface d'impression des briques

![](../images/briquevudessous.jpg)

puis par la suite configurer les paramètres de l'impression.

1. le reglage d'impression a 0.2mm

2. Couche et perimetre:

![](../images/coucheflix.jpg)

3. Remplissage:

![](../images/remplissageflix.jpg)

j'ai choisi d'autre paramètre pour les briques pour voir la difference

Couche et perimetre:

![](../images/couchebrique.jpg)

Remplissage:

![](../images/remplissagebrique.jpg)

4. Support: si besoin

![](../images/supportbrique.jpg)

Pour le reste de configurations sont restés comme configurer par defaut, mais il faudrait quand meme jeter un oeil pour verifier la compatibilité de ses parametres avec la machine mise a disposition.

pour finir exporter votre G-code dans une carte SD mise a disposition dans l'imprimante puis lancer l'impression on choisissant votre dossier (cliquez sur la molette a droite).

![](../images/briquegcode.jpg)

![](../images/flixgcode.jpg)

ci-dessous des photos prise au cours de la fabrication et a la fin

![](../images/briquefab.jpg)

![](../images/brique.jpg)

![](../images/flex.jpg)


### Useful links
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
